'use strict';

var pkg = require('./package.json');
var webpack = require('webpack');
var SafeUmdPlugin = require('safe-umd-webpack-plugin');
var isProduction = process.argv.indexOf('--production') >= 0;
var PolyfillsPlugin = require('webpack-polyfills-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var FILENAME = pkg.name + (isProduction ? '.min.js' : '.js');
var BANNER = [
    FILENAME,
    '@version ' + pkg.version,
    '@author ' + pkg.author,
    '@license ' + pkg.license
].join('\n');

var config = {
    devtool: 'source-map',
    entry: './src/js/index.js',
    output: {
        filename: './dist/' + FILENAME,
        libraryTarget: 'umd',
        library: ['tooltip']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(dist|node_modules|bower_components)/,
                loader: 'eslint-loader'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
            },
            {
                test: /[.png|.gif]$/,
                loader: 'url-loader'
            }
        ]
    },
    plugins: [
        new SafeUmdPlugin(),
        new webpack.BannerPlugin(BANNER),
        new PolyfillsPlugin([
            'Array/prototype/forEach',
            'Object/assign'
        ])

    ]
};

if (isProduction) {
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        compress: {
            'screw_ie8': false
        }
    }));
}

module.exports = config;
